class Stack {
    public:
        Stack(int _max_size);
        ~Stack();

        void push(int value);
        void pop(int value);

        bool isFull();
        bool isEmpty();

        void empty();

    private:
        int max_size;
        int length;

        int * data;
    
};