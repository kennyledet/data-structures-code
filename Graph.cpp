Graph::Graph(int _size) {
    adjacencyMatrix = new int[_size];
    size = _size;
};


Graph::~Graph( ) {
    delete []adjacencyMatrix;
};

void Graph::setAdjacency(int row, int col) {
    adjacencyMatrix[row][col] = 1;
    adjacencyMatrix[col][row] = 1;
};

