Stack::Stack(int _max_size) {
    max_size = _max_size;
    length = 0;

    // initialize Stack data array
    data = new int[_max_size];
}

void Stack::push(int value) {
    if ( ! data.isFull() )
        data[length] = value;

    length++;
}

void Stack::pop(int value) {
    if ( ! data.isEmpty() ) {
        int popped = data[length];

        data[length] = NULL;
        length--;

        return popped;
    }
}

bool isFull() {
    return data.size() >= max_size;
}

bool isEmpty() {
    return data.size() <= 0;
}