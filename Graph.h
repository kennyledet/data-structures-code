class Graph
{
    public:

        int * adjacency_matrix;

        Graph(int _size);
        ~Graph();

        void setAdjacency(int row, int col);

    private:
        int size;
        int length;
};