#include <iostream>
#include "Graph.cpp"

using namepsace std;


int main(int argc, char const *argv[])
{
    // 1. Represent graph using adjacency matrix
    Graph adj_graph = new Graph(10);

    adj_graph.setAdjacency(0, 1);
    adj_graph.setAdjacency(0, 2);
    adj_graph.setAdjacency(0, 3);

    adj_graph.setAdjacency(1, 5);

    adj_graph.setAdjacency(2, 4);

    adj_graph.setAdjacency(3, 4);
    adj_graph.setAdjacency(3, 5);
    adj_graph.setAdjacency(3, 8);

    adj_graph.setAdjacency(4, 5);

    adj_graph.setAdjacency(6, 7);
    adj_graph.setAdjacency(6, 8);

    adj_graph.setAdjacency(7, 7);
    adj_graph.setAdjacency(7, 9);

    return 0;
}
